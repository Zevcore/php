<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


class DistanceCalculator {

	/**
	 * @param array  $from
	 * @param array  $to
	 * @param string $unit - m, km
	 *
	 * @return mixed
	 */
	public function calculate( $from, $to, $unit = 'm' ) {
		var_dump($from, $to, $unit);

		// pierwiastek kwadratowy
        // (x2 - x1)^2
        // +
        // (y2 - y1)^2
		//return $distance;

        //x res
        $x_res = pow($from[1] - $from[0], 2);
        //y res
        $y_res = pow($to[1] - $to[0], 2);


        return sqrt($x_res - $y_res);
	}

	/**
	 * @param array $from
	 * @param array $offices
	 *
	 * @return array
	 */
	public function findClosestOffice( $from, $offices ) {
		var_dump($offices[1]);
		foreach($offices as $row) {
			$lat = $row['lat'];
			$lng = $row['lng'];
		}

		$x_res = pow($from[1] - $from[0], 2);
        //y res
        $y_res = pow($lng - $lat, 2);
		return sqrt($x_res - $y_res);

		//SELECT * FROM offices W
	}

}
